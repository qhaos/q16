; ascii in ax, out in al
decode:
	sub al, 0x30
	cmp al, 0x0A
	jl .endr1
	sub al, 0x07
	cmp al, 0x0F
	jl .endr1
	sub al, 0x20
.endr1:
	sub ah, 0x30
	cmp ah, 0x0A
	jl .endr2
	sub ah, 0x07
	cmp ah, 0x0F
	jl .endr2
	sub ah, 0x20
.endr2:
	shl ah, 4
	or al, ah
	ret

; str is in bx, length = cx*2 (cx holds the number of resultant bytes, as opposed to the actual size of the str)
decode_str:
	push ax
.loop:
	mov ah, [bx]
	inc bx
	mov al, [bx]
	call decode
	dec bx
	mov [bx], ax
	add bx, 2
	loop .loop
	pop ax
	ret

; str is in bx, length = cx*2 (fixed for now)
load:
	pusha
	push cx
	mov si, bx
	call decode_str
	mov bx, 0
	mov es, bx
	mov bx, 0x500
	pop cx
.loadloop:
	lodsw
	mov [es:bx], al
	inc bx
	loop .loadloop
	popa
	push ds
	push si
	xor ax, ax
	mov ds, ax
	mov si, ax
	call 0:0x500
	pop si
	pop ds
	ret
