main:
	; set video mode 0x3
	mov ax, 0x0003
	int 0x10

	mov cx, 0x02607
	mov ah, 1
	int 0x10

	mov ax, kmsg_kb
	call print
	call setup_sys
	mov ax, kmsg_done
	call print
.loop:
	mov ax, load_msg
	call print
	call gets
	mov bx, ax
	call load
	push ax
	mov ax, load_done
	call print
	pop ax
	call print_hex
	jmp .loop
halt:
	cli
	hlt
	jmp halt

kmsg_kb: db "Setting up keyboard handler...........", 0
kmsg_done: db "OK", 0x0
load_msg: db 0x0A,"Load your code here:", 0x0A, " > ", 0
load_done: db "Return value: ", 0

test_code: db 'bb06058a07c348'
%include "console.asm"
%include "input.asm"
%include "sys.asm"
%include "load.asm"
