; al returns the char, ah the scancode
getc:
	cli
	mov al, [kb_input_ready]
	or al, al
	jnz .end
	sti
.loop:
	mov al, [kb_input_ready]
	or al, al
	jz .loop
.end:
	mov [kb_input_ready], byte 0
	mov al, [kb_input_buff]
	mov ah, [kb_input_scan]
	push ax
	mov al, [echo]
	or al, al
	pop ax
	jz .noecho
	call putchar
.noecho:
	sti
	ret

; ax holds pointer to data (needs to be refactor ASAP), cx holds length of string
%define INPUT_BUFF 0x1000
gets:
	push bx
	mov bx, INPUT_BUFF
	xor cx, cx
.loop:
	call getc
	cmp al, 0x0A
	je .end
	cmp al, 0x0D
	je .loop
	cmp al, 0x08
	je .bkspc
	mov [bx], al
	inc bx
	inc cx
	jmp .loop
.end:
	mov [bx], byte 0
	mov ax, INPUT_BUFF
	pop bx
	ret
.bkspc:
	dec bx
	mov [bx], byte ' '
	jmp .loop

echo: db 1
%include "scancode.asm"
