%define VGA_SEG 0xB800
%define END_VIDEO 160*25

%macro clear 0
	mov dx, ax
	mov ax, 0x03
	int 10h
	mov ax, dx
%endmacro

print:
	pusha
	push es
	mov si, ax
	mov ax, VGA_SEG
	mov es, ax
	mov bx, [curr_pos]
	sub bx, 2
.loop:
	add bx, 2
	cmp bx, END_VIDEO
	jle .allgood
	clear
	mov bx, 0
.allgood:
	lodsb
	mov [.lastchar], al
	or al, al
	jz .end
	cmp al, 0x0A
	je .newline
	cmp al, 0x0D
	je .carrige
	cmp al, 0x08
	je .backspace
	mov ah, [color]
	mov [es:bx], ax
	jmp .loop
.end:
	mov [curr_pos], bx
	pop es
	popa
	ret
.newline:
	mov ax, bx
	xor bx, bx
	div byte [screen_width]
	mul byte [screen_width]
	mov bl, [screen_width]
	add ax, bx
	mov bx, ax
	sub bx, 2
	jmp .loop
.carrige:
	add bx, [screen_width]
	jmp .loop
.backspace:
	mov al, [.lastchar]
	cmp al, 0x0A
	jne .notnewline
	sub bx, [screen_width]
	jmp .loop
.notnewline:
	or bx, bx
	jnz .cmpbk
	sub bx, 2
	jmp .loop
.cmpbk:
	sub bx, 2
	mov byte [es:bx], ' '
	sub bx, 2
	jmp .loop
.lastchar: db 0


; number is in al
print_hexdigit:
	push bx
	and ax, 0x0F
	mov bx, hexstr
	add bx, ax
	mov ax, [bx]
	mov byte [.tmpstr], al
	mov ax, .tmpstr
	call print
	pop bx
	ret
	.tmpstr: dw 0

; number is in al
print_hex:
	push dx
	mov dl, al
	shr al, 4
	call print_hexdigit
	mov al, dl
	call print_hexdigit
	pop dx
	ret
; char is in al
putchar:
	push ax
	mov [.buff], al
	mov ax, .buff
	call print
	pop ax
	ret
.buff: dw 0

; x is in al
setx:
	push bx
	push ax
	mov ax, [curr_pos]
	div byte [screen_width]
	xor ah, ah
	mul byte [screen_width]
	pop bx
	add ax, bx
	mov [curr_pos], ax
	pop bx

hexstr: db '0123456789ABCDEF'
screen_width: db 160
screen_height: db 25
curr_pos: dw 0
color: db 0x02
