bits 16
org 0x7C00
	jmp 0x0000:boot
boot:

	xor ax, ax
	mov es, ax
	mov ds, ax
	mov ss, ax

	mov bp, 0x7C00
	mov sp, bp

	; int 0x13, disk services
	mov ah, 0x2    ; ah = 0x2 read
	mov al, 3      ; al = sectors to read
	mov ch, 0      ; cylinder
	mov dh, 0      ; head
	mov cl, 2      ; secotr
	mov bx, load_target
	int 0x13

	jmp 0:load_target


times 510 - ($-$$) db 0
dw 0xaa55
load_target:
	jmp main

%include "main.asm"
